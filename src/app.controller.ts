import { Controller, Get, Post, UseInterceptors, UploadedFiles, Param, Res } from '@nestjs/common';
import { AppService } from './app.service';
import {FilesInterceptor, FileFieldsInterceptor} from '@nestjs/platform-express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
// for single file -> FileInterceptor
// @UploadedFile()

  @Post('upload')
  @UseInterceptors(FilesInterceptor('image')) // file interceptor takes two arguments filename and multer options
  // this one takes array of files
  uploadFile(@UploadedFiles() file ){
  console.log(file)
  }
  
  @Get(':imgpath')
  seeUpldFile(@Param('imgpath') image,@Res() res){
    console.log(res,image)
  return res.sendFile(image,{root:'uploads'});
  }

@Post('multiple')
@UseInterceptors(FileFieldsInterceptor([
  {name:'img1',maxCount:1},
  {name:'backImg',maxCount:1} // this is to upload multiple fields with all different field name keys
]))
uploadMultiFiles(@UploadedFiles() files){
  console.log(files)
}


}
